import java.util.ArrayList;

public class DraftBookApp {
    public static void main(String[] args) {
        Note note1 = new Note("Maria", ".", "Oi.");
        Note note2 = new Note("Maria", "Sem título", "What a Wonderful Word é meu mangá favorito do Inio Asano até então.");

        ArrayList<Note> notes = new ArrayList<Note>();
        notes.add(note1);
        notes.add(note2);

        System.out.println("* NOTAS SALVAS:");
        for (Note note : notes) {
            System.out.println(String.format("\n[%s] - %s", note.getCreatedAt().toString(), note.getTitle()));
            System.out.println(String.format("\t%s", note.getText()));
            System.out.println(String.format("por %s\n", note.getAuthor()));
        }
    }
}